import React from 'react';
import {Link} from 'react-router-dom';
import { IoIosArrowRoundBack } from 'react-icons/io';

import './style.css'
import logo from '../../assets/logo-phidelis.png';


function Login(){
    return(
        <div className="container-form">
            
            <form>
               <Link to="/" className="goBack"><IoIosArrowRoundBack size={30} color="#366ABA"/></Link>
                <div className="title-form">
                    <h2>Login</h2>
                    <span>Não e possui uma conta?</span><a href=""> Cadastra-se</a>
                </div>
                <div>
                    <input type="email" placeholder="Email"/>

                </div>
                <div>
                    <input type="password" placeholder="Senha"/>
                </div>
                <button type="submit">Entrar</button>
                <a href="">Esqueci minha senha</a>
                <img src={logo} alt="identidade Visual"/>
            </form>
        </div>
    );
}

export default Login;