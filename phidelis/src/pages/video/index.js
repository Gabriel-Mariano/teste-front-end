import React from 'react';
import './style.css';
import {Link} from 'react-router-dom';

function Video(){
    return(
        <div className="container-video">
            <video controls>
                
            </video>
            <Link to="/"><button>Fechar</button></Link>
        </div>
    );
}

export default Video;