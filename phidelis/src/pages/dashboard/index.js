import React from 'react';
import Header from '../../components/header';
import './style.css';

import { Link }from 'react-router-dom';



function Dashboard(){
    return(
        <>
        <Header data={{ nameIni:'Início', nameSai:'Sair' }}/>

        <section className="container-dashboard">
            
            <div className="container-dash">
            <h2>Dashboard</h2>
                <div>
                    <Link to="/publish"><button>Publicações</button></Link>
                    <Link to="/states"><button>Estados</button></Link>
                </div>
                <div>
                <Link to="/slider"><button>Slider</button></Link>
                <Link to="/users"><button>Usuários</button></Link>
                </div>
            </div>
        </section>
        </>
    )
}

export default Dashboard;