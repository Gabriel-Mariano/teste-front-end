import React from 'react';
import Header from '../../components/header';
import Slider from '../../components/slider';
import BoxSearch from '../../components/box-search';
import SectionLocation from '../../components/location';
import Footer from '../../components/footer';


function Home(){
    return(
        <>
        <Header data={{ nameIni:'Home', nameSai:'Login' }}/>
        <Slider/>
        <BoxSearch/>
        <SectionLocation/>
        <Footer/>
        </>
    );
}

export default Home;