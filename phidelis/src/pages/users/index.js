import React, {useContext} from 'react';
import {MyContext} from '../../config/context';
import { Link } from 'react-router-dom';
import Header from '../../components/header';
import './style.css';

import {FiTrash2} from 'react-icons/fi'; 
import {RiPencilLine} from 'react-icons/ri';


function Publish(){
    const { data } = useContext(MyContext); 
    return(
        <>
        <Header data={{ nameIni:'Início', nameSai:'Sair' }}/>

        <section className="container-dashboard">
            <h2>Dashboard</h2>
            <Link to="/">Inicio</Link><span> &gt; Usuários</span> 
            
            <div>
                <button className="add-btn">Adicionar Novo</button>
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Usuários</th>
                        <th>E-mail</th>
                        <th>Ações</th>
                    </tr>
                    <tbody>
                        {data.users.map((value,i)=>{
                            return (
                                <tr>
                                    <td>{value.id}</td>
                                    <td>{value.user}</td>
                                    <td>{value.email}</td>
                                    <td>
                                        <button><FiTrash2 color="#366ABA"/></button>
                                        <button><RiPencilLine color="#366ABA"/></button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </section>
        </>
    )
}

export default Publish;