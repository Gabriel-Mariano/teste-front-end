import React from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import './style.css';
import Convento from '../../assets/convento-da-penha.png';
import Avatar from '../../assets/avatar.png';


function Post(){
    return(
        <>
        <Header data={{ nameIni:'Home', nameSai:'Login' }}/>
        <div className="container-banner">
            <div className="content-title">
                <div>
                    <h1>Conheça o convento da penha por dentro</h1>
                </div>
                <div>
                    <img src={Convento} alt="Convento da Penha"/>
                </div>
            </div>
            <div className="content-post">
                <div className="content-avatar">
                    <div>
                        <img src={Avatar} alt="avatar" id="avatar"/>
                    </div>
                    <div>
                        <span><strong>Ana Furtado</strong></span>
                        <span>Estagiária</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="content">
            <span>
            Etiam eget ligula in dolor posuere vestibulum ullamcorper vitae ex. Pellentesque quis dolor malesuada, varius mi suscipit, egestas mauris. Sed non quam nunc. Ut eu nibh lectus. Donec pretium turpis nec fermentum malesuada. Praesent ultricies gravida leo ac cursus. Nullam vel quam sed urna fringilla blandit. Integer vehicula nulla eros, vel condimentum leo faucibus sit amet. Proin mattis rutrum eros. Proin vitae justo ex. Ut ultricies vehicula nunc eu facilisis. Maecenas gravida odio nec risus molestie, vel interdum nisl placerat. Nunc eget blandit magna, quis luctus eros. Etiam eget ligula in dolor posuere vestibulum ullamcorper vitae ex. Pellentesque quis dolor malesuada, varius mi suscipit, egestas mauris. Sed non quam nunc. Ut eu nibh lectus. Donec pretium turpis nec fermentum malesuada. - Praesent ultricies gravida leo ac cursus. - Nullam vel quam sed urna fringilla blandit. - Integer vehicula nulla eros, vel condimentum leo faucibus sit amet. - Proin mattis rutrum eros. Vestibulum facilisis elit nibh, eu laoreet ipsum fermentum eget. Pellentesque ornare erat id ligula blandit scelerisque. Praesent condimentum malesuada est eget tincidunt. Nunc purus nisl, commodo non finibus quis, eleifend eu elit. Integer consectetur libero sed erat aliquam feugiat. Suspendisse id maximus dui, sed fermentum justo.
            </span>
            <figure>
                <img src={Convento} alt="convento da penha"/>
                <figcaption>Convento da Penha</figcaption>
            </figure>
            <span>
            Maecenas ac nisi efficitur, maximus metus et, dictum turpis.Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum sed arcu vehicula, feugiat felisquis, viverra tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris molestie, neque ut malesuada pharetra, enim neque vehicula magna, egestas congue odio tortor eu magna. Duis facilisis, enim laoreet pulvinar cursus, ante dolor bibendum libero, quis iaculis elit ante a nunc.
            </span>
        </div>
        <Footer/>
        </>
    );
}

export default Post;