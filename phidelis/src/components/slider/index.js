import React from 'react';
import './style.css';
import {Link} from 'react-router-dom';

import {FaPlay} from 'react-icons/fa';

function Slider(){
    return(
        <>
            <section className="banner">
                <div className="content-banner">
                    <h1>Descubra diversos lugares e as melhores experiências aqui!</h1>
                    <div className="content-button">
                        <Link to="/video">
                            <button><FaPlay size={30} color="#366ABA" /></button>
                        </Link><span>Assita o vídeo</span>
                    </div>
                </div>
            </section>
            <section className="footer-banner">
                <h2>Viaje conosco pelo Brasil</h2>
                <span>O objetivo do blog é compartilhar as experiências vivenciadas nas viagens pelo Brasil.   
                </span>
            </section>
        </>
    );
}

export default Slider;