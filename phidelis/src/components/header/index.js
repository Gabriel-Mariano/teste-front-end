import React from 'react';
import {Link} from 'react-router-dom';
import './style.css';


import Logo from '../../assets/logo-phidelis.png';

function Header(props){
    return(
        <header className="content-header">
            <img src={Logo} alt="identidade Visual"/>
            <nav>
                <ul>
                    <li className="isActive"><Link to="/">{props.data.nameIni}</Link></li>
                    <li><Link to="/login">{props.data.nameSai}</Link></li>
                </ul>
            </nav>
        </header>
    );
}

export default Header;