import React, { useState, useContext } from 'react';
import './style.css';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io' ;

import {MyContext} from '../../config/context';

function Location() {
  const { data } = useContext(MyContext);
  const [location,setLocation] = useState('Todos');

function handleSearch(e){
    e.preventDefault();
    
    if(e.target.value === 'Todos'){
      setLocation('Todos');
    }else{
      setLocation(e.target.value);
    }

}

  return (
      <>
      <div className="container-location">
          <div className="nav-buttons">
              <div>
                <h1>Viagens por Estado</h1>
              </div>
              <div>
                  <button><IoIosArrowForward size={15}/></button>
                  <button><IoIosArrowBack size={15}/></button>
              </div>
          </div>
          <div className="container-uf">
              <button className="isActiveItems" onClick={handleSearch} value={'Todos'}>Todos</button>
                {data.states.map((value,i)=>{
                    return <button className="" key={i} onClick={handleSearch} value={value.state} >{value.state}</button>
                })}
          </div>
      </div>
      <div className="container-trips">
              <h1>Principais Viagens</h1>
              <div className="content-trips">
                { 
                   data.publish.map((v,i)=>{
                        return v.local === location ? 
                           <div key={i}>{v.title}</div> :
                           location === 'Todos' ?
                           <div key={i}>{v.title}</div> :
                           ''
                            
                            
                   })
                }  
                
              </div>
          </div>
      </>
  );
}

export default Location;
