import React from 'react';
import './style.css';
import {BiSearch} from 'react-icons/bi';


function BoxSearch(){
    
    return(
        <div className="container-search">
            <h1>Comece sua busca</h1>
            <hr></hr>
            <div className="content-input">
                <input type="text" placeholder="Pra onde você quer ir?"/>
                <BiSearch />
                
            </div>
        </div>
    );
}

export default BoxSearch;