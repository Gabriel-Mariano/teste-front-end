import React, {useState, createContext } from 'react';

export const MyContext = createContext();

export default function MyContextProvider({children}){
    const [data,setData] = useState({
            publish:[
                {
                    id:1,
                    title:'Conheça o convento da penha por dentro',
                    author:'Ana Furtado',
                    local:'Espirito Santo'
                },
                {
                    id:2,
                    title:'Conheça os lençois maranhenses',
                    author:'Ana Furtado',
                    local:'Maranhão'
                },
                {
                    id:3,
                    title:'O natal luz espetacular de Domingo Martins',
                    author:'Ana Furtado',
                    local:'Espirito Santo'
                }
            ],
            states:[
                {
                    id:1,
                    state:'São Paulo',
                    uf:'SP' 
                },
                {
                    id:2,
                    state:'Espirito Santo',
                    uf:'ES'
                },
                {
                    id:3,
                    state:'Rio de Janeiro',
                    uf:'RJ'
                },
                {
                    id:4,
                    state:'Minas Gerais',
                    uf:'MG'
                },
                {
                    id:5,
                    state:'Ceará',
                    uf:'CE'
                },
                {
                    id:6,
                    state:'Bahia',
                    uf:'BA'
                }
            ],
            slider:[
                {
                    id:1,
                    title:'Conheça o convento da penha por dentro',
                }
            ],
            users:[
                {
                    id:1,
                    user:'Ana Furtado',
                    email:'ana@gmail.com'
                },
                {
                    id:2,
                    user:'Ana Furtado',
                    email:'ana@gmail.com'
                }
            ]

    });
    return(
        <MyContext.Provider value={{data,setData}}>
            {children}
        </MyContext.Provider>
    );
}