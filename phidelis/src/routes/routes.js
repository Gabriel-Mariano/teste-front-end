import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import Home from '../pages/home';
import Login from '../pages/login';
import Publish from '../pages/publish';
import States from '../pages/states';
import Slider from '../pages/slider';
import Users from '../pages/users';
import Dashboard from '../pages/dashboard';
import Video from '../pages/video';
import Post from '../pages/post';

export default function Routes(){
    return(
        <Router>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/publish" component={Publish}/>
                <Route exact path="/states" component={States}/>
                <Route exact path="/slider" component={Slider}/>
                <Route exact path="/users" component={Users}/>
                <Route exact path="/dashboard" component={Dashboard}/>
                <Route exact path="/post" component={Post}/>
                <Route exact path="/video" component={Video}/>
            </Switch>
        </Router>
    );
}