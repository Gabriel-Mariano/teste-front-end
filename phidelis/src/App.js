import './App.css';
import Home from './pages/home';
import MyContext from './config/context';
import Routes from './routes/routes';


function App() {
  return (
    <MyContext>
      <Routes/>
    </MyContext>
  );
}

export default App;
